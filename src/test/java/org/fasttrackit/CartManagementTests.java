package org.fasttrackit;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertFalse;

public class CartManagementTests {

        Page page = new Page();
        Header header = new Header();

        CartPage cartPage = new CartPage();
        @BeforeClass
        public void setup() {
            page.openHomePage();

        }

        @AfterMethod
        public void cleanup() {
            Footer footer = new Footer();
            footer.clickToReset();
            header.clickOnTheShoppingBagIcon();
        }

    @Test
    @Description("When user navigates to Cart Page, empty cart page message is displayed")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 012")
    public void when_user_navigates_to_cart_page_empty_cart_page_message_is_displayed(){
     header.clickOnTheCartIcon();
        Assert.assertEquals(cartPage.getEmptyCartPageText(),"How about adding some products in your cart?");

    }

    @Test
    @Description("Adding one product to Cart, empty cart page message is not shown")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 013")
    public void adding_one_product_to_cart_empty_cart_message_is_not_shown(){
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());

    }

    @Test
    @Description("User can increment the amount of a product in cart page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 014")
    public void user_can_increment_the_amount_of_a_product_in_cart_page(){
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem ("9");
        item.increaseAmount();
        Assert.assertEquals(item.getItemAmount(), "2");

    }
    @Test
    @Description("User can reduce the amount of a product in cart page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 015")
    public void user_can_reduce_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("9");
        item.reduceAmount();
        Assert.assertEquals(item.getItemAmount(), "1");

    }
    }

