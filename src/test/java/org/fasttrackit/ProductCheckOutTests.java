package org.fasttrackit;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

public class ProductCheckOutTests {
        Page page = new Page();
        ProductCards productList = new ProductCards();

        Header header = new Header();
        CartPage cartPage = new CartPage();

        CheckOutPage checkOutPage = new CheckOutPage();
      OrderSummaryPage orderSummaryPage = new OrderSummaryPage();

        @BeforeClass
        public void setup () {
            page.openHomePage();
        }
        @AfterMethod
        public void cleanup () {
            Footer footer = new Footer();
            footer.clickToReset();

        }

        @Test()
        @Description("User can navigate to Checkout Page from Cart Page")
        @Severity(SeverityLevel.NORMAL)
        @Owner("Laura Faur")
        @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
        @Issue("DMS 020")
        public void user_can_navigate_to_checkout_page_from_cart_page(){
            page.openHomePage();
            Product product = new Product( "9");
            product.clickOnTheProductCartIcon();
            header.clickOnTheCartIcon();
            cartPage.clickOnTheCheckOutButton();

            Assert.assertEquals(checkOutPage.getTitle(), "Your information", "Your information");
        }

    @Test()
    @Description("User can navigate from Checkout Page back to Cart Page")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 021")
    public void user_can_navigate_from_checkout_page_back_to_cart_page(){
        page.openHomePage();
        Product product = new Product( "9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckOutButton();
        checkOutPage.clickOnTheCancelButton();

        Assert.assertEquals(cartPage.getCartPageTitle(), "Your cart", "Your cart");
    }

    @Test()
    @Description("User can navigate from Checkout Page to Homepage")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 022")
    public void user_can_navigate_from_checkout_page_to_Homepage(){
        page.openHomePage();
        Product product = new Product( "9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckOutButton();
        header.clickOnTheShoppingBagIcon();
        Assert.assertEquals(page.getPageTitle(), "Products", "Homepage is displayed.");
    }
    @Test()
    @Description("User can navigate from Checkout Page to Wishlist")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 023")
    public void user_can_navigate_from_checkout_page_to_Wishlist(){
        page.openHomePage();
        Product product = new Product( "9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckOutButton();
        header.clickOnTheWishlistIcon();
        Assert.assertEquals(page.getPageTitle(), "Wishlist", "Wishlist is displayed.");
    }
    @Test()
    @Description("User can not continue checkout if first name field is not filled")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 024")
    public void user_can_not_continue_checkout_if_first_name_field_is_not_filled(){
        page.openHomePage();
        Product product = new Product( "9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckOutButton();
        checkOutPage.clickOnTheContinueCheckoutButton();
        Assert.assertEquals(page.getPageTitle(), "Your information", "Remained on the Checkout page, warning displayed.");
        }

    @Test()
    @Description("User can not continue checkout if last name field is not filled")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 025")
    public void user_can_not_continue_checkout_if_last_name_field_is_not_filled(){
        page.openHomePage();
        Product product = new Product( "9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckOutButton();
        checkOutPage.typeInFirstName();
        checkOutPage.clickOnTheContinueCheckoutButton();
        Assert.assertEquals(page.getPageTitle(), "Your information", "Remained on the Checkout page, warning displayed.");
    }
    @Test()
    @Description("User can not continue checkout if address field is not filled")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 026")
    public void user_can_not_continue_checkout_if_address_field_is_not_filled(){
        page.openHomePage();
        Product product = new Product( "9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckOutButton();
        checkOutPage.typeInFirstName();
        checkOutPage.typeInLastName();
        checkOutPage.clickOnTheContinueCheckoutButton();
        Assert.assertEquals(page.getPageTitle(), "Your information", "Remained on the Checkout page, warning displayed.");

    }
    @Test()
    @Description("User can continue checkout if address information is complete")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 027")
    public void user_can_continue_checkout_if_address_information_is_complete(){
        page.openHomePage();
        Product product = new Product( "9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckOutButton();
        checkOutPage.typeInFirstName();
        checkOutPage.typeInLastName();
        checkOutPage.typeInAddress();
        checkOutPage.clickOnTheContinueCheckoutButton();
        Assert.assertEquals(orderSummaryPage.getOrderSummaryPageTitle(), "Order summary", "User is on Order summary page");

    }

    @Test()
    @Description("User can complete an order")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 028")
    public void user_can_complete_an_order() {
        page.openHomePage();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckOutButton();
        checkOutPage.typeInFirstName();
        checkOutPage.typeInLastName();
        checkOutPage.typeInAddress();
        checkOutPage.clickOnTheContinueCheckoutButton();
        checkOutPage.clickOnTheCompleteYourOrderButton();
        Assert.assertEquals(checkOutPage.getOrderCompletePageTitle(), "Order complete", "Thank you for your order!");
    }
    @Test()
    @Description("User can continue ahopping after a complete order")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 029")
    public void user_can_continue_shopping_after_a_complete_order() {
        page.openHomePage();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckOutButton();
        checkOutPage.typeInFirstName();
        checkOutPage.typeInLastName();
        checkOutPage.typeInAddress();
        checkOutPage.clickOnTheContinueCheckoutButton();
        checkOutPage.clickOnTheCompleteYourOrderButton();
        checkOutPage.clickOnTheContinueShoppingButton();
        Assert.assertEquals(page.getPageTitle(), "Products", "Homepage is diplayed");
    }
    @Test()
    @Description("User can cancel an order")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 030")
    public void user_can_cancel_an_order() {
        page.openHomePage();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckOutButton();
        checkOutPage.typeInFirstName();
        checkOutPage.typeInLastName();
        checkOutPage.typeInAddress();
        checkOutPage.clickOnTheContinueCheckoutButton();
        checkOutPage.clickOnTheCancelButton();
        Assert.assertEquals(cartPage.getCartPageTitle(), "Your cart", "Cart page with product added is displayed");

    }
}
