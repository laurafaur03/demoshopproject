package org.fasttrackit;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;


public class InitialTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();


    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickToReset();

    }

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @Test
    @Description("User dino can login with valid credentials")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 001")
    public void user_dino_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetinMessage(), "Hi dino!", "Logged in with dino, expected greetings message to be Hi dino!");
    }

    @Test
    @Description("User turtle can login with valid credentials")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 002")
    public void user_turtle_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetinMessage(), "Hi turtle!", "Logged in with turtle, expected greetings message to be Hi turtle!");
    }

    @Test
    @Description("User beetle can login with valid credentials")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 003")
    public void user_beetle_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetinMessage(), "Hi beetle!", "Logged in with beetle, expected greetings message to be Hi beetle!");
    }

    @Test
    @Description("User can navigate to Wishlist Page")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 004")
    public void user_can_navigate_to_Wishlist_Page() {
        header.clickOnTheWishlistIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on the Wishlist page.");
    }

    @Test
    @Description("User can navigate to Cart Page")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 005")
    public void user_can_navigate_to_cart_Page() {
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart page");
    }

    @Test
    @Description("User can navigate to Home Page from Wishlist Page")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 006")
    public void user_can_navigate_to_Home_Page_from_Wishlist_Page() {
        header.clickOnTheWishlistIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Products page.");
    }

    @Test
    @Description("User can navigate to Home Page from Cart Page")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 007")
    public void user_can_navigate_to_Home_Page_from_Cart_Page() {
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Products page");
    }

    @Test
    @Description("User can navigate to Wishlist Page from Cart Page")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 008")
    public void user_can_navigate_to_WishListPage_from_Cart_Page() {
        header.clickOnTheCartIcon();
        header.clickOnTheWishlistIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on Wishlist page");
    }

    @Test
    @Description("User can add product to cart from product cards")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 009")
    public void user_can_add_product_to_cart_from_product_cards() {
        ProductCards cards = new ProductCards();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");

    }

    @Test
    @Description("User can add two products to cart from product cards")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 010")
    public void user_can_add_two_products_to_cart_from_product_cards() {
        ProductCards cards = new ProductCards();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two products to cart, badge shows 2.");
    }

    @Test
    @Description("User can add three products to cart from product cards")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 011")
    public void user_can_add_three_products_to_cart_from_product_cards() {
        ProductCards cards = new ProductCards();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "3", "After adding two products to cart, badge shows 3.");
    }


}
