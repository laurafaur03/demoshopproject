package org.fasttrackit;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ProductSortingTest {

    Page page = new Page();
    ProductCards productList = new ProductCards();

    @BeforeClass
    public void setup () {
        page.openHomePage();
    }
    @AfterMethod
    public void cleanup () {
        Footer footer = new Footer();
        footer.clickToReset();

    }

@Test
@Description("When sorting products A to Z, products are sorted alphabetically")
@Severity(SeverityLevel.NORMAL)
@Owner("Laura Faur")
@Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
@Issue("DMS 016")
    public void when_sorting_products_a_to_z_products_are_sorted_alphabetically() {
    Product firstProductBeforeSort = productList.getFirstProductInList();
    Product lastProductBeforeSort = productList.getLastProductInList();
    productList.clickOnTheSortButton();
    productList.clickOnTheAZSortButton();
    Product firstProductAfterSort = productList.getFirstProductInList();
    Product lastProductAfterSort = productList.getLastProductInList();

    assertEquals(firstProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
    assertEquals( lastProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());

    }
    @Test
    @Description("When sorting products Z to A, products are sorted alphabetically DESC")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 017")
    public void when_sorting_products_Z_to_A_products_are_sorted_alphabetically_DESC() {
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheZASortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
        assertEquals( lastProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());

    }
    @Test
    @Description("When sorting products by price low to high, products are sorted by price low to high")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 018")
    public void when_sorting_products_by_price_low_to_high_products_are_sorted_by_price_low_to_high() {
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
        assertEquals( lastProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());

    } @Test
    @Description("When sorting products by price high to low, products are sorted by price high to low")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Laura Faur")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS 019")
    public void when_sorting_products_by_price_high_to_low_products_are_sorted_by_price_high_to_low() {
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceHiLo();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());

    }
}

