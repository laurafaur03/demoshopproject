package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.devtools.v85.media.model.PlayerErrorsRaised;

import java.net.SocketOption;

import static com.codeborne.selenide.Selenide.$;


public class CheckOutPage {

    private final SelenideElement checkOutPageTitle = $(".text-muted");
    private SelenideElement cancelButton = $(".btn-danger");
    private final SelenideElement continueCheckoutButton = $(".btn-success");


    private final SelenideElement firstNameField = $("#first-name");
    private final SelenideElement lastNameField = $("#last-name");
    private final SelenideElement addressField = $("#address");
    private final SelenideElement completeYourOrderButton = $(".btn-success");
    private final SelenideElement orderCompletePageTitle = $(".text-muted");

    private final SelenideElement continueShoppingButton = $(".btn-success");

    public String getTitle() {
        return checkOutPageTitle.text();
    }

    public void clickOnTheCancelButton() {
        cancelButton.click();
    }

    public void clickOnTheContinueCheckoutButton() {
        continueCheckoutButton.click();
    }

    public void clickOnFirstNameField() {
        firstNameField.click();
    }

    public void typeInFirstName() {
        String FirstName = "Laura";
        firstNameField.type(FirstName);

    }

    public void clickOnTheLastNameField() {
        lastNameField.click();
    }

    public void typeInLastName() {
        String LastName = "Faur";
        lastNameField.type(LastName);

    }

    public void clickOnTheAddressField() {
        addressField.click();
    }
@Step("Type in address.")
    public void typeInAddress() {
        String Address = "Ocna Sugatag";
        addressField.type(Address);

    }
@Step("Click on the complete your order button.")
    public void clickOnTheCompleteYourOrderButton() {
        completeYourOrderButton.click();
    }

    public String getOrderCompletePageTitle() {
        return orderCompletePageTitle.text();
    }

    @Step("Click on the continue shopping button.")
    public void clickOnTheContinueShoppingButton() {
        continueShoppingButton.click();
    }



}
