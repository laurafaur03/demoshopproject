package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class OrderSummaryPage {

    private final SelenideElement OrderSummaryPageTitle = $(".subheader-container") ;

    public String getOrderSummaryPageTitle() {
        return OrderSummaryPageTitle.text();
    }
}
