package org.fasttrackit;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Header {
private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");

    private final SelenideElement greetingElement = $(".navbar-text span span");
    private final SelenideElement wishlistButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");




   public static  String url = Page.URL;
@Step("Click on the login Button.")
    public void clickOnTheLoginButton()
    {
        loginButton.click();
        System.out.println("Click on the Login button.");
    }

    public String getGreetinMessage () {

        return greetingElement.text();
    }


    @Step("Click on the wishlist Button.")
    public void clickOnTheWishlistIcon() {
        System.out.println("Click on the Wishlist button.");

        wishlistButton.click();
    }
    @Step("Click on the Shopping bag icon.")
    public void clickOnTheShoppingBagIcon() {
        System.out.println( " Click on the shopping bag icon.");
        homePageButton.click();
        url = Page.URL;
    }

    @Step("Click on the cart icon.")
    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart Icon");
        cartIcon.click();
    }
    public String getShoppingCartBadgeValue(){
        return this.shoppingCartBadge.text();
    }
}
