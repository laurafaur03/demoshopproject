package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private SelenideElement emptyCartPageElement = $(".text-center");
    private String emptyCardPage;

    private SelenideElement checkOutButton = $(".btn-success");
    private SelenideElement cartPageTitle = $(".text-muted");


    public CartPage() {

    }

    public String getEmptyCartPageText() {
        return this.emptyCartPageElement.text();
    }
    public boolean isEmptyCartMessageDisplayed(){
        return emptyCartPageElement.isDisplayed();
    }
@Step("Click on the checkout button.")
    public void clickOnTheCheckOutButton(){
        checkOutButton.click();
    }

    public String getCartPageTitle(){
        return cartPageTitle.text();
    }





}
